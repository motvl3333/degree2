#include <engine/entity.h>

void process_entity_tile_collision(struct Entity *p_entity, SPRT *p_tile)
{
	// horizontal collision checking
	if(p_entity->velocity.x < 0)
	{
		if(colbox_is_horizontally_aligned(&p_entity->colbox, p_tile))
		{
			/*  check if left side of entity colbox WOULD pass the right side of
			 *  maptile horizontally with current velocity AND collision box is
			 *  still to the right of maptile
			 */
			if(	(p_entity->colbox.x0 <  ((p_tile->x0 + p_tile->w) - p_entity->velocity.x)) &&
				(p_entity->colbox.x0 >=  (p_tile->x0 + p_tile->w)) )
			{
				// fix the velocity according to the difference of entity and tile
				p_entity->velocity.x = (p_tile->x0 + p_tile->w) - p_entity->colbox.x0;
			}
		}
	}
	else if(p_entity->velocity.x > 0)
	{
		if(colbox_is_horizontally_aligned(&p_entity->colbox, p_tile))
		{
			/*  check if right side of entity colbox WOULD pass the left side of
			 *  maptile horizontally with current velocity AND collision box is
			 *  still to the left of maptile
			 */
			if(	(p_entity->colbox.x2 > (p_tile->x0 - p_entity->velocity.x)) &&
				(p_entity->colbox.x2 <= p_tile->x0) )
			{
				// fix the velocity according to the difference of the entity and tile
				p_entity->velocity.x = (p_tile->x0 - p_entity->colbox.x2);
			}
		}

	}

	// vertical collision checking
	if(p_entity->velocity.y < 0)
	{
		// can't be on the floor if you're going up
		p_entity->is_on_floor = 0;

		if(colbox_is_vertically_aligned(&p_entity->colbox, p_tile))
		{
			/*  check if top of entity colbox WOULD pass the bottom side of
			 *  maptile vertically with current velocity AND collision box is
			 *  still to the bottom of maptile
			 */
			if(	(p_entity->colbox.y0 <  (p_tile->y0 + p_tile->h) - p_entity->velocity.y) &&
				(p_entity->colbox.y0 >= (p_tile->y0 + p_tile->h)) )
			{
				// fix the velocity according to the difference of the entity and tile
				p_entity->velocity.y = (p_tile->y0 + p_tile->h) - p_entity->colbox.y0;
			}
		}
	}
	else if(p_entity->velocity.y > 0)
	{
		if(colbox_is_vertically_aligned(&p_entity->colbox, p_tile))
		{
			/*  check if bottom of entity colbox WOULD pass the top side of
			 *  maptile vertically with current velocity AND collision box is
			 *  still to the top of maptile
			 */
			if(	(p_entity->colbox.y2 > (p_tile->y0 - p_entity->velocity.y)) &&
				(p_entity->colbox.y2 <= p_tile->y0))
			{
				// fix the velocity according to the difference of the entity and tile
				p_entity->velocity.y = (p_tile->y0 - p_entity->colbox.y2);
				p_entity->is_on_floor = 1;
			}
			else
			{
				p_entity->is_on_floor = 0;
			}
		}
		else
		{
			p_entity->is_on_floor = 0;
		}
	}
}

