#ifndef ALLOCMEM_H
#define ALLOCMEM_H

#include <stdio.h>
#include <stdlib.h>

void init_allocmem_system(void);

// allocates memory of a specified size in allocbuf
// if possible. it returns a NULL pointer upon failure.
char *allocmem(unsigned int nobj, unsigned int size);

// frees the piece of allocated memory associated
// with the memory address specified by addr. does
// nothing if an invalid pointer is passed.
void freemem(char *p_addr);

// prints the memory addresses and sizes of all pieces
// of allocated memory. used for debugging.
void seemem(void);

#endif

