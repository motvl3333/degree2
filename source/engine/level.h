#ifndef LEVEL_H
#define LEVEL_H

#include <engine/transform.h>
#include <engine/entity.h>
#include <engine/camera.h>

#define MAX_ROWS	80
#define MAX_COLS	160

// spacing between frames of an animation in a texture page
#define TILE_SPACING		1
// all tiles must be 16x16
#define TILE_SPRITE_SIZE	16

// tiles that are rendered on-screen applied to them for
// player and enemy entities. Any off-screen entities will
// will have their velocities set to 0 since collision is
// not processed for off-screen tiles
#define TILES_RENDERED_HORIZONTALLY	24
#define TILES_RENDERED_VERTICALLY	18

// screen render limits
#define RENDER_LEFT_SIDE_LIMIT		0
#define RENDER_RIGHT_SIDE_LIMIT		MAX_COLS - TILES_RENDERED_HORIZONTALLY

#define RENDER_TOP_SIDE_LIMIT		0
#define RENDER_BOTTOM_SIDE_LIMIT	MAX_ROWS - TILES_RENDERED_VERTICALLY

#define UNIQUE_TILES	225	// number of different tiles in a 256x256px texture
#define AIR_TILE	0	// 0 is always the air tile

// a "section" is the playable area of a level. A level usually consists of multiple sections.
// Sections can be separated via doors but can still be contained within a single level. Since
// the Section struct takes up quite a bit of memory, only one Section struct (on_screen_section)
// is stored in main memory. To store more than one section in memory, SectionHeaders are used.
struct Section
{
	// a map is a 2D array of integers that correspond to the indexes of the 'tileset' member of a level.
	// the value of each element of the map serves as an index into the tileset array to determine the
	// UV values of each SPRT!
	unsigned char		(*p_current_map)[MAX_COLS];		// pointer to the 2D map being used

	struct Vector2x 	offset;
	struct Vector2u		tileset[UNIQUE_TILES];			// uv coordinates for the actual sprites to be rendered on-screen
	SPRT			p_sprite_layout[MAX_ROWS][MAX_COLS];	// array of actual sprites to be rendered on-screen
	DR_TPAGE		dr_tpage;				// tpage for each SPRT
}on_screen_section;	// only this section should be used, but you can create more if you want. A good
			// example would be if you needed another Section but with a "pretty" tilemap that
			// doesn't have collision. i.e. flowers, paintings, walls, clouds, etc..

struct SectionHeader
{
	struct Vector2x		offset;
	struct Vector2x		*p_enemy_spawn;		// pointer to array of enemy coords
	unsigned char		*p_map;			// pointer to map associated with this SectionHeader
};

struct LevelHeader
{
	struct SectionHeader	*p_section;		// pointer to array of SectionHeaders
	unsigned char		section_headers_used;
};

struct LevelHeader *p_level;			// pointer to array of LevelHeaders

// current level/section numbers (0-indexed)
unsigned short current_level, current_section;

char init_section(struct Section *p_section, unsigned char (*p_map)[MAX_COLS], struct Vector2x *p_offset, struct Vector2w *p_sprite_size, unsigned short clut, DR_TPAGE *dr_tpage);
void draw_section(struct Section *p_section, struct Camera *p_camera);
void move_section(struct Section *p_section, struct Camera *p_camera);

#endif

