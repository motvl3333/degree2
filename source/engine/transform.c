#include <engine/psx.h>
#include <engine/transform.h>

#include <res/circle_NTSC.h>
#include <res/circle_PAL.h>

void ROTATE_POLYFT4(POLY_FT4 *p_poly, struct Vector2x *p_center, char v_is_flipped, char h_is_flipped, unsigned int deg, unsigned int radius)
{
	// indexes to circle arrays in circle_NTSC.h or circle_PAL.h.
	// both x_coords and y_coords arrays correspond with the same
	// index!
	short x0, x1, x2, x3;
	short y0, y1, y2, y3;

	int array_size = SECOND*2;

	int deg_per_index = (4096*360)/array_size;

	if((x0 = deg/deg_per_index) >= array_size)
	{
		while(x0 >= array_size)
		{
			x0 -= array_size;
		}
	}

	// if HFLIPPED and VFLIPPED or NEITHER
	// (x0, y0)
	//    |
	//    V
	// (x1, y1)
	//    |
	//    V
	// (x3, y3)
	//    |
	//    V
	// (x2, y2)
	if((h_is_flipped && v_is_flipped) || (!h_is_flipped && !v_is_flipped))
	{
		x1 = x0;

		// the next coordinate proceeds 1/4 along the circle.
		// if it goes above 360 deg, wrap around
		if((x1 += array_size/4) >= array_size)
		{
			x1 -= array_size;
		}

		x3 = x1;

		if((x3 += array_size/4) >= array_size)
		{
			x3 -= array_size;
		}

		x2 = x3;

		if((x2 += array_size/4) >= array_size)
		{
			x2 -= array_size;
		}

#if REGION == NTSC_DISC
		p_poly->x0 = NTSC_x_coords[x0]* (radius/16) + p_center->x;
		p_poly->x1 = NTSC_x_coords[x1]* (radius/16) + p_center->x;
		p_poly->x2 = NTSC_x_coords[x2]* (radius/16) + p_center->x;
		p_poly->x3 = NTSC_x_coords[x3]* (radius/16) + p_center->x;
#else
		p_poly->x0 = PAL_x_coords[x0]* (radius/16) + p_center->x;
		p_poly->x1 = PAL_x_coords[x1]* (radius/16) + p_center->x;
		p_poly->x2 = PAL_x_coords[x2]* (radius/16) + p_center->x;
		p_poly->x3 = PAL_x_coords[x3]* (radius/16) + p_center->x;
#endif
	}

	// ELSE,
	// (x0, y0)
	//    |
	//    V
	// (x2, y2)
	//    |
	//    V
	// (x3, y3)
	//    |
	//    V
	// (x1, y1)
	else
	{
		x2 = x0;

		// the next coordinate proceeds 1/4 along the circle.
		// if it goes above 360 deg, wrap around
		if((x2 += array_size/4) >= array_size)
		{
			x2 -= array_size;
		}

		x3 = x2;

		if((x3 += array_size/4) >= array_size)
		{
			x3 -= array_size;
		}

		x1 = x3;

		if((x1 += array_size/4) >= array_size)
		{
			x1 -= array_size;
		}

#if REGION == NTSC_DISC
		p_poly->x0 = NTSC_x_coords[x0]* (radius/16) + p_center->x;
		p_poly->x1 = NTSC_x_coords[x1]* (radius/16) + p_center->x;
		p_poly->x2 = NTSC_x_coords[x2]* (radius/16) + p_center->x;
		p_poly->x3 = NTSC_x_coords[x3]* (radius/16) + p_center->x;
#else
		p_poly->x0 = PAL_x_coords[x0]* (radius/16) + p_center->x;
		p_poly->x1 = PAL_x_coords[x1]* (radius/16) + p_center->x;
		p_poly->x2 = PAL_x_coords[x2]* (radius/16) + p_center->x;
		p_poly->x3 = PAL_x_coords[x3]* (radius/16) + p_center->x;
#endif
	}

	if((y0 = deg/deg_per_index) >= array_size)
	{
		while(y0 >= array_size)
		{
			y0 -= array_size;
		}
	}

	// if HFLIPPED and VFLIPPED or NEITHER
	// (x0, y0)
	//    |
	//    V
	// (x1, y1)
	//    |
	//    V
	// (x3, y3)
	//    |
	//    V
	// (x2, y2)
	if((h_is_flipped && v_is_flipped) || (!h_is_flipped && !v_is_flipped))
	{
		y1 = y0;

		// the next coordinate proceeds 1/4 along the circle.
		// if it goes above 360 deg, wrap around
		if((y1 += array_size/4) >= array_size)
		{
			y1 -= array_size;
		}

		y3 = y1;

		if((y3 += array_size/4) >= array_size)
		{
			y3 -= array_size;
		}

		y2 = y3;

		if((y2 += array_size/4) >= array_size)
		{
			y2 -= array_size;
		}

#if REGION == NTSC_DISC
		p_poly->y0 = NTSC_y_coords[y0]* (radius/16) + p_center->y;
		p_poly->y1 = NTSC_y_coords[y1]* (radius/16) + p_center->y;
		p_poly->y2 = NTSC_y_coords[y2]* (radius/16) + p_center->y;
		p_poly->y3 = NTSC_y_coords[y3]* (radius/16) + p_center->y;
#else
		p_poly->y0 = PAL_y_coords[y0]* (radius/16) + p_center->y;
		p_poly->y1 = PAL_y_coords[y1]* (radius/16) + p_center->y;
		p_poly->y2 = PAL_y_coords[y2]* (radius/16) + p_center->y;
		p_poly->y3 = PAL_y_coords[y3]* (radius/16) + p_center->y;
#endif
	}
	// ELSE,
	// (x0, y0)
	//    |
	//    V
	// (x2, y2)
	//    |
	//    V
	// (x3, y3)
	//    |
	//    V
	// (x1, y1)
	else
	{
		y2 = y0;

		// the next coordinate proceeds 1/4 along the circle.
		// if it goes above 360 deg, wrap around
		if((y2 += array_size/4) >= array_size)
		{
			y2 -= array_size;
		}

		y3 = y2;

		if((y3 += array_size/4) >= array_size)
		{
			y3 -= array_size;
		}

		y1 = y3;

		if((y1 += array_size/4) >= array_size)
		{
			y1 -= array_size;
		}

#if REGION == NTSC_DISC
		p_poly->y0 = NTSC_y_coords[y0]* (radius/16) + p_center->y;
		p_poly->y1 = NTSC_y_coords[y1]* (radius/16) + p_center->y;
		p_poly->y2 = NTSC_y_coords[y2]* (radius/16) + p_center->y;
		p_poly->y3 = NTSC_y_coords[y3]* (radius/16) + p_center->y;
#else
		p_poly->y0 = PAL_y_coords[y0]* (radius/16) + p_center->y;
		p_poly->y1 = PAL_y_coords[y1]* (radius/16) + p_center->y;
		p_poly->y2 = PAL_y_coords[y2]* (radius/16) + p_center->y;
		p_poly->y3 = PAL_y_coords[y3]* (radius/16) + p_center->y;
#endif
	}
}

