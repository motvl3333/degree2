#ifndef COLLISION_H
#define COLLISION_H

// note: the collision box does NOT use allocmem, so
// you can get rid of it by exiting the scope it was
// allocated in.

#include <engine/psx.h>
#include <engine/transform.h>

// collision box:
//              line x0     line x1     line x2
//
//                 ^           ^           ^
//                 |           |           |
//                 |           |           |
// line y0 <-------boxboxboxboxboxboxboxboxb------->
//                 o           |           o
//                 x           |           x
//                 b           |           b
// line y1 <-------o-----------|-----------o------->
//                 x           |           x
//                 b           |           b
//                 o           |           o
// line y2 <-------boxboxboxboxboxboxboxboxx------->
//                 |           |           |
//                 |           |           |
//                 V           V           V

struct CollisionBox
{
	short			x0, y0, x1, y1, x2, y2;
	char			enable_debug;

	// debug members
	LINE_F2			left_boundary;
	LINE_F4			boundary;

	//                                ______________ 
	//                               |              |
	//                               v              |
	//                        __________________    |  
	//                       |                  |   |  
	//                       |                  |   |  
	//                       |                  |   |  
	//   left_boundary ----> |                  | <--- boundary
	//                       |                  |   |  
	//                       |                  |   |  
	//                       |__________________|   |  
	//                               ^              |  
	//                               |              |  
	//                               |______________|  
	//                                           

	LINE_F2 debug_x0, debug_y0;
	LINE_F2 debug_x1, debug_y1;
	LINE_F2 debug_x2, debug_y2;
};

// useful for collision between collision boxes and level geometry
char colbox_is_horizontally_aligned(struct CollisionBox *p_colbox, SPRT *p_tile);
char colbox_is_vertically_aligned(struct CollisionBox *p_colbox, SPRT *p_tile);

void init_collision_box(struct CollisionBox *p_colbox, struct ShortRECT *p_rect);
void move_collision_box(struct CollisionBox *p_colbox, short x, short y);
void draw_collision_box(struct CollisionBox *p_colbox);

#endif

