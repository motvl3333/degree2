#ifndef INIT_H
#define INIT_H

#include <engine/psx.h>
#include <engine/display.h>
#include <engine/controls.h>
#include <engine/asset.h>
#include <engine/allocmem.h>

#include <game/gamestate.h>

void init(); // initialize everything

//////////////// display init ////////////////
void init_graphics();
void init_double_buffer();

//////////////// controls init ////////////////
void init_controller();

#endif

